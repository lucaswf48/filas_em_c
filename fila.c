#include <stdio.h>
#include <stdlib.h>
#include "fila.h"



Fila *filaCria(){
	Fila *f = (Fila *)malloc(sizeof(Fila));
	f->ini = f->fim = NULL;
	return f;
}

void filaInsere(Fila *f,float v){

	ListaFila *n = (ListaFila *)malloc(sizeof(ListaFila));
	n->info = v;
	n->prox = NULL;
	if(f->fim != NULL)
		f->fim->prox = n;
	else
		f->ini = n;
	f->fim = n;

}

float filaRetira(Fila *f){

	ListaFila *t;
	float v;
	if(filaVazia(f)){
		printf("Vazia\n");
		exit(1);
	}
	t = f->ini;
	v = t->info;
	f->ini = t->prox;
	if(f->ini ==NULL)
		f->fim = NULL;
	free(t);
	return v;
}

int filaVazia(Fila *f){

	return (f->ini == NULL);
}

void filaLibera(Fila *f){

	ListaFila *q = f->ini;
	while(q != NULL){

		ListaFila *t = q->prox;
		free(q);
		q = t;
	}
	free(f);
}
void filaImprime(Fila *f){
	ListaFila *q;
	for(q = f->ini; q!= NULL; q = q->prox)
		printf("%d \n", q->info);
}
