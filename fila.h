

struct listafila{
	int info;
	struct lista *prox;
};

typedef struct listafila ListaFila;

struct fila{

	ListaFila *ini;
	ListaFila *fim;
};

typedef struct fila Fila;


Fila *filaCria();
void filaInsere(Fila *f, float v);
float filaRetira(Fila *f);
int filaVazia(Fila *f);
void filaLibera(Fila *f);
int filaIncr(int i);
